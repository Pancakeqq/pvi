var SelectedRow;
const addStudent = ()=>{
    var table = document.getElementById("StudTable");
    var row = table.insertRow(1);
    row.innerHTML = `<td class = "lables"><input type="checkbox"></td>
    <td class = 'lables'>PZ-25</td>
    <td class = 'lables'>Peter Morgan</td>
    <td class = 'lables'>Male</td>
    <td class = 'lables'>10.12.2004</td>
    <td class = 'lables'><i class="fa-solid fa-circle"></i></td>
    <td><button  class="deleteButton"  onclick="deleteConfirmation(event)">
    <i  class="fa-solid fa-xmark"></i></button >
    <button  class="deleteButton"">
    <i  class="fa-solid fa-pen"></i>
    </button ></td>`
}

const ShowAddWindow =()=>{
    var drop = document.createElement('div')
    var window = document.createElement('div')
    drop.id = "backdrop"
    window.id = "window"
    window.innerHTML = `<div class="window">
    <div>
    <h3 style ="padding:20px">Add Student</h3><button  class="closeButton" onclick="CloseAddWindow()"><i class="fa-solid fa-xmark"></i></button>
    </div>
    <div class="window-inner">Future form</div>
    <div class = "window-options-box">
    <input class = "window-options" type ="button" value = "ok" onclick = "addStudent(),CloseAddWindow() ">
    <input class = "window-options" type ="button" value = "cancel" onclick = "CloseAddWindow() ">
    </div> </div>`;
    document.body.appendChild(drop)
    document.body.appendChild(window)
}

const deleteConfirmation = (event) =>{
    event.stopPropagation()
    SelectedRow = event.target.parentNode.parentNode;
    var drop = document.createElement('div')
    var window = document.createElement('div')
    drop.id = "backdrop"
    window.id = "window"
    window.innerHTML = `<div class="window">
    <div>
    <h3 style ="padding:20px">Warning</h3><button  class="closeButton" onclick="CloseAddWindow()"><i class="fa-solid fa-xmark"></i></button>
    </div>
    <div class="window-inner">Are you sure want to delete user?</div>
    <div class = "window-options-box">
    <input class = "window-options" type ="button" value = "ok" onclick = "removeRow(),CloseAddWindow() ">
    <input class = "window-options" type ="button" value = "cancel" onclick = "CloseAddWindow() ">
    </div> </div>`;
    document.body.appendChild(drop)
    document.body.appendChild(window)
}


const CloseAddWindow = () =>{
 var drop = document.getElementById("backdrop")
 var window = document.getElementById("window")
 var body = document.getElementById("App")

 body.removeChild(drop)
 body.removeChild(window)
}

const removeRow = ()=>{

 var table = SelectedRow.parentNode;
 table.removeChild(SelectedRow)
}

const rotate = () =>{
    var bell = document.getElementById('bell');
    bell.classList.toggle('animated')
}